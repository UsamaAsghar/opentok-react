import React, { Component } from 'react';

import axios from 'axios';
import { OTSession, OTStreams, preloadScript } from '../../src';
import { OTWhiteBoard } from 'opentok-react-whiteboard';
import ConnectionStatus from './ConnectionStatus';
import Publisher from './Publisher';
import Subscriber from './Subscriber';
import config from '../config';
const apiKey = '';
const sessionId = '';
const token = '';
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      connected: false,
      apiKey: '',
      sessionId: '',
      token: '',
      credsFetched: false
    };

    this.sessionEvents = {
      sessionConnected: () => {
        this.setState({ connected: true });               
      },
      sessionDisconnected: () => {
        this.setState({ connected: false });
      }
    };
    this.server = this.server.bind(this);
  }

  server () {
    var SERVER_BASE_URL = 'https://784380c7.ngrok.io';
    // var SERVER_BASE_URL = 'https://nuraniah-opentok-rtc.herokuapp.com';
    axios.get(SERVER_BASE_URL + '/otok/create-session')
      .then((res) => {
        const { sessionId, generatedToken } = res.data;
        console.log('res----->>>>>', res);
        this.setState({
          apiKey: '46480622',
          sessionId,
          token: generatedToken,
          credsFetched: true
        })
        OT.registerScreenSharingExtension('chrome', config.CHROME_EXTENSION_ID, 2);
        this.initializeSession('46480622',sessionId)
      })
      .catch(function (error) {
        console.log('errrrro===>>>>', error);
      });

  }

  handleError(error) {
    if (error) {
      alert(error.message);
    }
  }

  initializeSession(apiKey,sessionId) {
    var session = OT.initSession(apiKey, sessionId);
    console.log('session init===>>>',session);
    // Subscribe to a newly created stream
    session.on('streamCreated', function (event) {
      session.subscribe(event.stream, 'subscriber', {
        insertMode: 'append',
        width: '100%',
        height: '100%'
      }, this.handleError);
    });
  
    // Create a publisher
    var publisher = OT.initPublisher('publisher', {
      insertMode: 'append',
      width: '100%',
      height: '100%'
    }, this.handleError);
  
    // Connect to the session
    session.connect(token, function (error) {
      // If the connection is successful, publish to the session
      if (error) {
        this.handleError(error);
      } else {
        session.publish(publisher, this.handleError);
      }
    });
  }

  componentWillMount() {
    this.server();
    // OT.registerScreenSharingExtension('chrome', config.CHROME_EXTENSION_ID, 2);
  }

  onError = (err) => {
    this.setState({ error: `Failed to connect: ${err.message}` });
  }

  render() {
    const { apiKey, sessionId, token, credsFetched } = this.state;
    return (
      <div>
        {credsFetched &&
          <OTSession
            apiKey={apiKey}
            sessionId={sessionId}
            token={token}
            eventHandlers={this.sessionEvents}
            onError={this.onError}
          >
            <OTWhiteBoard />
            {this.state.error ? <div>{this.state.error}</div> : null}
            <ConnectionStatus connected={this.state.connected} />
            <Publisher />
            <OTStreams>
              <Subscriber />
            </OTStreams>
          </OTSession>
        }
      </div>
    );
  }
}

export default preloadScript(App);
